# Package Version10\Utils

Ce dépôt a pour objectif de centraliser tout le code écrit par Version 10 et qui n'est pas spécifique à un projet.
Autrement dit, un code qui peut être réutilisé dans plusieurs projets.

Le premier but est de remplacer notre vieux dossier `utils` par des classes et de s'en servir comme dépendances dans nos projets.
Par exemple, le package `version10/utils-php` va devenir une dépendence du CMS et de certains projets (les sites web par exemple).

Cette dépendance sera gérée dans les projets via [Composer](https://getcomposer.org) :
```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/version10/utils-php"
        }
    ],
    "require": {
        "version10/utils-php": "dev-master"
    }
}
```
