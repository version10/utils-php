<?php

namespace Version10\Utils\Mysql;

class DBStructureGetterTest extends \PHPUnit_Framework_TestCase
{
    /** The database service */
    private $db;

    /** Database settings */
    private $dbSettings;

    protected function setUp()
    {
        if (!defined('BDD_HOST')) {
            define('BDD_HOST', '192.168.0.2');
            define('BDD_PORT', '3306');
            define('BDD_USER', 'root_local');
            define('BDD_PASS', 'ei875rgj97E_$$lJN');
            define('BDD_NAME', 'tlmep_simultane');
        }

        $this->dbSettings = array (
            'BDD_HOST' => BDD_HOST,
            'BDD_PORT' => BDD_PORT,
            'BDD_USER' => BDD_USER,
            'BDD_PASS' => BDD_PASS,
            'BDD_NAME' => BDD_NAME
        );

        $this->db = Bdd::getInstance();
    }

    protected function tearDown()
    {
        $this->dbSettings = null;
        $this->db = null;
    }


    public function testGetDump()
    {
        $myDBStructureGetter = new DBStructureGetter($this->db, $this->dbSettings);

        $this->assertNotNull($myDBStructureGetter->getDump());
    }

    /**
     * @expectedException Exception
     */
    public function testValidateTables()
    {
        $tables = array('this_table_does_not_exists');

        new DBStructureGetter($this->db, $this->dbSettings, $tables);
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testConstructWrongArgs1()
    {
        new DBStructureGetter();
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testConstructWrongArgs2()
    {
        new DBStructureGetter(null, null, "my_table");
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testConstructWrongArgs3()
    {
        new DBStructureGetter($this->db, $this->dbSettings, "my_table");
    }
}
