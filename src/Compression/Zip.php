<?php

namespace Version10\Utils\Compression;

/**
 *
 */

class Zip
{
    /**
     * Creates a compressed zip file
     * @param array of string $files the files to compress
     * @param string $destination destination of the zip file
     * @param bool $overwrite if true, overrides if a file alreday exists
     * @return boolean
     */
    public function createZip($files = array(), $destination = '', $overwrite = false)
    {
        // If the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }

        // Vars
        $valid_files = array();

        // If files were passed in...
        if (is_array($files)) {
            // Cycle through each file
            foreach ($files as $file) {
                // Make sure the file exists
                if (file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }

        // If we have good files...
        if (count($valid_files)) {
            // Create the archive
            $zip = new ZipArchive();

            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }

            // Add the files
            foreach ($valid_files as $file) {
                $zip->addFile($file, $file);
            }

            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            // Close the zip -- done!
            $zip->close();

            // Check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    /**
     * Envoie le fichier $filepath au client et le supprime du serveur
     * @param string $filepath chemin du fichier ZIP
     * @param string $filename nom du fichier envoyé au client
     * @return boolean false si le fichier n'existe pas, true si tout s'est bien déroulé
     */
    public function sendAndUnlinkZipFile($filepath, $filename = null)
    {
        if (!file_exists($filepath)) {
            echo 'Le fichier n\'existe pas.';
            return false;
        }

        if (is_null($filename)) {
            $filename = basename($filepath);
        }

        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Content-Length: '.filesize($filepath));
        readfile($filepath);

        unlink($filepath);

        return true;
    }
}
