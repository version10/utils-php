<?php

namespace Version10\Utils\Mobile;

/**
 * Utils related to the Date and Time formatage
 */

class IsMobile
{
	public static function isMobileBrowser()
	{
		if (!empty($_SERVER['HTTP_USER_AGENT'])) {
			return strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone')
				|| strstr($_SERVER['HTTP_USER_AGENT'], 'iPod')
				|| strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')
				|| strstr($_SERVER['HTTP_USER_AGENT'], 'Windows Phone')
				|| strstr($_SERVER['HTTP_USER_AGENT'], 'Android')
				|| strstr($_SERVER['HTTP_USER_AGENT'], 'BlackBerry');
		}

		return false;
	}

	public static function isMobileBrowserBy($pB)
	{
		if (!empty($_SERVER['HTTP_USER_AGENT'])) {
			return strstr($_SERVER['HTTP_USER_AGENT'], $pB);
		}

		return false;
	}

	public function checkUserAgent($type = null)
	{
	    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	    if ($type == 'bot') {
	        // matches popular bots
	        if (preg_match("/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent)) {
	                return true;
	                // watchmouse|pingdom\.com are "uptime services"
	        }
	    } elseif ($type == 'browser') {
	        // matches core browser types
	        if (preg_match("/mozilla\/|opera\//", $user_agent)) {
	                return true;
	        }
	    } elseif ($type == 'mobile') {
	        // matches popular mobile devices that have small screens and/or touch inputs
	        // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
	        // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
	        if (preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)) {
	                // these are the most common
	                return true;
	        } elseif (preg_match("/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent)) {
	                // these are less common, and might not be worth checking
	                return true;
	        }
	    }

	    return false;
	}
}
