<?php

namespace Version10\Utils\ImageProcessing;

/**
 * Permet au CMS de manipuler des images
 */
class Image
{
    public $file;
    public $image_width;
    public $image_height;
    public $width;
    public $height;
    public $ext;
    public $types = array('', 'gif', 'jpeg', 'png');
    public $quality = 96;
    public $top = 0;
    public $left = 0;
    public $crop = false;
    public $type;
    public $newtype = 'jpg';
    public $fitIn = false; // Si on demande de faire les manip automatique
    public $bw = false; // Noir et blanc
    public $sepia = false;
    public $img_res = null; // Utilisé uniquement si on load une image depuis le web

    protected $stringService;

    public function __construct($name = '')
    {
        if (empty($name)) {
            trigger_error('Nom du fichier vide!', E_USER_ERROR);
            exit();
        }

        $stringService = new \Version10\Utils\StringFormat\StringService();

        // Permet de charger des images directement depuis le web
        // TODO jpeg only now, étendre à tous les formats
        if ($stringService->startsWithHttpProtocol($name)) {
            if (strpos($name, '?') !== false) {
                $this->file = substr($name, 0, strpos($name, '?'));
            } else {
                $this->file = $name;
            }

            // Selon le type d'image, on appelle la bonne fonction de création d'image
            $fileExtension = getExtension(sanitizeExtension($this->file), false);

            switch ($fileExtension) {
                case 'jpg':
                    $this->img_res = imagecreatefromjpeg($this->file);
                    break;
                case 'png':
                    $this->img_res = imagecreatefrompng($this->file);
                    break;
                case 'gif':
                    $this->img_res = imagecreatefromgif ($this->file);
                    break;
                default:
                    trigger_error("Extension d'image non gérée par la classe Image.", E_USER_ERROR);
                    exit();
            }

            // Check création de l'image
            if (!$this->img_res) {
                trigger_error("Erreur lors de la création de l'image depuis le lien.", E_USER_ERROR);
                exit();
            }

            $this->image_width = imagesx($this->img_res);
            $this->image_height = imagesy($this->img_res);
            $this->name = strtolower(removeExtension(basename($this->file)));

            if ($fileExtension == 'jpg') {
                $this->type = 'jpeg';
            } else {
                $this->type = $fileExtension;
            }

            $this->dir = '';
        } else {
            $this->file = $name;
            $info = getimagesize($name);
            $this->image_width = $info[0];
            $this->image_height = $info[1];
            $this->type = $this->types[$info[2]];
            $info = pathinfo($name);
            $this->dir = $info['dirname'];
            $this->name = str_replace('.'.$info['extension'], '', $info['basename']);
        }

        $this->ext = $this->newtype;
    }

    /**
     * Set ou get le repertoire
     * @param string $dir
     * @return rien | string le nom du répertoire
     */
    public function dir($dir = '')
    {
        if (!$dir) {
            return $this->dir;
        }

        $this->dir = $dir;
    }

    public function name($name = '')
    {
        if (!$name) {
            return $this->name;
        }

        $this->name = $name;
    }

    public function newtype($newtype = '')
    {
        if (!$newtype) {
            return $this->newtype;
        }

        if (strtolower($newtype) == 'jpeg') {
            $newtype = 'jpg';
        }

        $this->newtype = strtolower($newtype);
        $this->ext = strtolower($newtype);
    }

    public function width($width = '')
    {
        if (!$width) {
            return $this->width;
        }

        $this->width = $width;
    }

    public function height($height = '')
    {
        if (!$height) {
            return $this->height;
        }

        $this->height = $height;
    }

    public function resize($percentage = 50)
    {
        if ($this->crop) {
            $this->crop = false;
            $this->width = round($this->width * ($percentage / 100));
            $this->height = round($this->height * ($percentage / 100));
            $this->image_width = round($this->width / ($percentage / 100));
            $this->image_height = round($this->height / ($percentage / 100));
        } else {
            $this->width = round($this->image_width * ($percentage / 100));
            $this->height = round($this->image_height * ($percentage / 100));
        }
    }

    public function fitIn($fWidth, $fHeight)
    {
        $this->fitIn = true;
        $ratio = $fWidth / $this->image_width;
        $ratio2 = $this->image_width / $fWidth;

        if (round($this->image_height * $ratio) > $fHeight) {
            $this->left = 0;
            $this->top = (($this->image_height * $ratio) - $fHeight) * $ratio2 / 2;
            $this->image_height = $this->image_height - ($this->top * 2);
        } else {
            $ratio = $fHeight / $this->image_height;
            $ratio2 = $this->image_height / $fHeight;
            $this->top = 0;
            $this->left = (($this->image_width * $ratio) - $fWidth) * $ratio2 / 2;
            $this->image_width = $this->image_width - ($this->left * 2);
        }

        $this->height = $fHeight;
        $this->width = $fWidth;
    }

    public function bw($bool)
    {
        $this->bw = (bool)$bool;
    }

    public function sepia($bool)
    {
        $this->sepia = (bool)$bool;
        $this->bw = true;
    }

    public function crop($top = 0, $left = 0)
    {
        $this->crop = true;
        $this->top = $top;
        $this->left = $left;
    }

    public function quality($quality = 80)
    {
        $this->quality = $quality;
    }

    public function show()
    {
        $this->save(true);
    }

    public function save($show = false)
    {
        set_time_limit('60');
        ini_set('memory_limit', '256M');

        if ($show == true) {
            @header('Content-Type: image/'.$this->type);
        }

        if (!$this->width && !$this->height) {
            $this->width = $this->image_width;
            $this->height = $this->image_height;
        } elseif (is_numeric($this->width) && empty($this->height)) {
            $this->height = round($this->width / ($this->image_width / $this->image_height));
        } elseif (is_numeric($this->height) && empty($this->width)) {
            $this->width = round($this->height / ($this->image_height / $this->image_width));
        } else {
            if ($this->width <= $this->height) {
                $height = round($this->width / ($this->image_width / $this->image_height));

                if ($height != $this->height) {
                    $percentage = ($this->image_height * 100) / $height;
                    $this->image_height = round($this->height * ($percentage / 100));
                }
            } else {
                $width = round($this->height / ($this->image_height / $this->image_width));

                if ($width != $this->width) {
                    $percentage = ($this->image_width * 100) / $width;
                    $this->image_width = round($this->width * ($percentage / 100));
                }
            }
        }

        if ($this->crop && !$this->fitIn) {
            $this->image_width = $this->width;
            $this->image_height = $this->height;
        }

        if ($stringService->startsWithHttpProtocol($this->file)) {
            $image = $this->img_res;
        } elseif ($this->type == 'jpeg') {
            $image = imagecreatefromjpeg($this->file);
        } elseif ($this->type == 'png') {
            $image = imagecreatefrompng($this->file);
        } elseif ($this->type == 'gif') {
            $image = imagecreatefromgif($this->file);
        }

        $new_image = imagecreatetruecolor($this->width, $this->height);

        // Setting alpha blending off
        imagealphablending($new_image, false);

        if ($this->type == 'png') {
            // Save alphablending setting (important)
            imagesavealpha($new_image, true);
        } elseif ($this->type == 'gif') {
            // TODO Gestion des gifs tranparents
        }

        imagecopyresampled(
            $new_image, $image,
            0, 0, $this->left, $this->top,
            $this->width, $this->height, $this->image_width, $this->image_height
        );

        // Noir et blanc?
        if ($this->bw) {
            imagefilter($new_image, IMG_FILTER_GRAYSCALE);
        }

        // Sépia?
        if ($this->sepia == true) {
            imagefilter($new_image, IMG_FILTER_COLORIZE, 40, 30, 0);
        }

        if ($this->dir[strlen($this->dir) - 1] != '/') {
            $this->dir .= '/';
        }

        if ($show == true) {
            $name = null;
        } else {
            $name = $this->dir.$this->name.'.'.$this->ext;
        }

        // Sauvegarde selon le type
        if (strtolower($this->newtype) == 'jpg' || strtolower($this->newtype) == 'jpeg') {
            imagejpeg($new_image, $name, $this->quality);
        } elseif (strtolower($this->newtype) == 'png') {
            imagepng($new_image, $name);
        } elseif (strtolower($this->newtype) == 'gif') {
            imagegif($new_image, $name);
        }

        // Cleaning
        imagedestroy($image);
        imagedestroy($new_image);
    }

    public function destroy()
    {
        imagedestroy($this->img_res);
    }
}
