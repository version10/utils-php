<?php

namespace Version10\Utils\Webservices;

/**
 * Utils related to strings format
 */
class CakeAPI
{
    private $apikey;
    private $clientid;
    private $apiurl = 'https://api.wbsrvc.com';
    private $user_key;
    private $campaign_id;
    private $mailing_id;

    public function __construct($apikey, $clientid)
    {
        $this->apikey = $apikey;
        $this->clientid = $clientid;
    }

    public function call($url, $params)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl.$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey:'.$this->apikey));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        if ($result === false) {
            throw new Exception('Curl error: '.curl_error($ch));
        } else {
            if (!$result = json_decode($result)) {
                curl_close($ch);
                throw new Exception('API Key Validation Error for '.$this->apikey.'. Contact your administrator!');
            }

            curl_close($ch);

            if ($result->status != 'success') {
                throw new Exception($result->data);
            }
        }

        return $result->data;
    }

    public function login($username, $password, $clientId)
    {
        $config = array(
            'email'     => $username,
            'password'  => $password,
            'client_id' => $this->clientid
        );

        if ($result = $this->call('/User/Login/', $config)) {
            $this->userkey = $result->user_key;

            return $this->userkey;
        } else {
            return false;
        }
    }

    public function addMember($list_id, $record, $optIn = false)
    {
        $config = array(
            'user_key'  => $this->userkey,
            'list_id'   => $list_id,
            'record'    => $record
        );

        if ($optIn) {
            $config['email'] =  $user_email;

            if ($result = $this->call('/List/SubscribeEmail/', $config)) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($result = $this->call('/List/Import/', $config)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function createCampaign($name)
    {
        $configCampaign = array(
            'user_key' => $this->userkey,
            'name' => $name,
        );

        if ($result = $this->call('/Campaign/Create/', $configCampaign)) {
            return $result;
        }

        return false;
    }

    public function createMail($name, $list_id, $subject, $date, $contentHTML, $sender_email = '', $sender_name = '')
    {
        $config = array(
            'user_key'          => $this->userkey,
            'name'              => $name,
            'type'              => 'standard',
            'encoding'          => 'utf-8',
            'transfer_encoding' => 'base64'
        );

        if ($result = $this->call('/Mailing/Create/', $config)) {
            $mailing_id = $result;

            $configSetInfo = array(
                'user_key'          => $this->userkey,
                'mailing_id'        => $mailing_id ,
                'list_id'           => $list_id,
                'subject'           => utf8_encode($subject),
                'html_message'      => $contentHTML,
                'sender_email'      => $sender_email,
                'sender_name'       => utf8_encode($sender_name),
                'reply_to'          => $sender_email,
                'opening_stats'     => 'true',
                'clickthru_html'    => 'true'
            );

            $result = $this->setMailInfo($configSetInfo);

            $configSchedule = array(
                'user_key'      => $this->userkey,
                'mailing_id'    => $mailing_id ,
                'date'          => $date,
            );

            $result = $this->scheduleMail($configSchedule);

            return true;
        }

        return false;
    }

    public function setMailInfo($config)
    {
        if ($result = $this->call('/Mailing/SetInfo/', $config)) {
            return true;
        }

        return false;
    }

    public function scheduleMail($configSchedule)
    {
        if ($result = $this->call('/Mailing/Schedule/', $configSchedule)) {
            return true;
        }

        return false;
    }

    // Envoi d'un mail de test
    public function testMail($testMail, $infoSubject, $senderEmail, $infoName, $contentHTML)
    {
        $config = array(
            'user_key'      => $this->userkey,
            'email'         => $testMail,
            'encoding'      => 'utf-8',
            'subject'       => $infoSubject,
            'sender_email'  => $senderEmail,
            'sender_name'   => $infoName,
            'html_message'  => $contentHTML,
        );

        if ($result = $this->call('/Relay/Send/', $config)) {
            return true;
        }

        return false;
    }

    public function rappelMail($testMail, $infoSubject, $senderEmail, $infoName, $contentHTML)
    {
        $config = array(
            'user_key'      => $this->userkey,
            'email'         => $testMail,
            'encoding'      => 'utf-8',
            'subject'       => $infoSubject,
            'sender_email'  => $senderEmail,
            'sender_name'   => $infoName,
            'html_message'  => $contentHTML,
        );

        if ($result = $this->call('/Relay/Send/', $config)) {
            return true;
        }

        return false;
    }
}
