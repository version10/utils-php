<?php

namespace Version10\Utils\Cache;

class CacheApc {

    public $timeToLive;

    function __construct($ttl = 600) {
        $this->timeToLive = $ttl;
    }

    /**
     * Obtient la valeur de la clé demandé dans la cache APC
     * @param  string $key Clé de la mémoire
     * @return string|null
     */
    function getData($key) {
        $success = false;
        $data = apc_fetch($key, $success);

        return ($success) ? $data : null;
    }

    /**
     * Définie la valeur de la clé demandée dans la cache APC
     * @param string $key  Nom de la clé
     * @param string|array Valeur de la clé
     */
    function setData($key, $data) {
        return apc_store($key, $data, $this->timeToLive);
    }

    /**
     * Supprime la clé de la cache APC
     * @param  string $key Nom de la clé
     * @return bool        Résultat de l'opération
     */
    function delData($key) {
        return (apc_exists($key)) ? apc_delete($key) : true;
    }


}
