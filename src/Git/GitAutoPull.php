<?php

namespace Version10\Utils\Git;

/**
 * This class is useful to automatically update a read-only .git repo
 * through a webservice called by Bitbucket when some code is pushed to the remote repo
 *
 * Usage :
 * $gitAutoPull = new GitAutoPull('dev', true);
 * $gitAutoPull->pullIt();
 *
 */
class GitAutoPull
{
    private $branch;
    private $outputToFile;

    /**
     * Constructor
     */
    public function __construct($branch = 'dev', $outputToFile = false)
    {
        $this->branch = $branch;
        $this->outputToFile = $outputToFile;
    }

    /**
     * Triggers the following shell commands:
     * 1. cd ../
     * 2. git checkout [branch]
     * 3. git fetch --all
     * 4. git reset --hard origin/[branch]
     *
     * and outputs the result to the page or to a file
     */
    public function pullIt()
    {
        $now = date("Y-m-d_H-i-s");

        $shellCommands = "cd ../ && git checkout " . $this->branch . " && git fetch --all && git reset --hard origin/" . $this->branch;

        $output = array();
        $output['timestamp'] = $now;

        exec($shellCommands, $output['exec_output'], $output['return_var']);

        if ($this->outputToFile) {
            $file = 'git-pull-log-' . $now . '.log';
            file_put_contents($file, print_r($output, true), FILE_APPEND | LOCK_EX);
        } else {
            echo '<pre>';
            print_r($output);
            echo '</pre>';
        }
    }
}
