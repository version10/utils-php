<?php

namespace Version10\Utils\Mysql;

use Version10\Utils\OS\Helper as OSHelper;

/**
 * This class can be used to get the structure of a MySQL database
 *
 * Exemple usage :
 * TODO
 *
 */
class DBStructureGetter
{
    /** The database service */
    private $db;

    /** Database settings */
    private $dbSettings;

    /** The tables we want to get the structure */
    private $tables;

    /**
     * Constructor
     */
    public function __construct(Bdd $Bdd, array $dbSettings, array $tablesNames = array())
    {
        $this->db = $Bdd;
        $this->dbSettings = $dbSettings;

        $this->validateTables($tablesNames);
    }

    /**
     *
     */
    public function getDump()
    {
        if (!OSHelper::commandExists("mysqldump")) {
            throw new \Exception("Command mysqldump is not available.");
        }

        /*if (!OSHelper::commandExists("sed")) {
            throw new \Exception("Command sed is not available.");
        }*/

        /*$command = "mysqldump --no-data --skip-comments -h " . $this->dbSettings['BDD_HOST'] . " -u " . $this->dbSettings['BDD_USER'] . " -p" . $this->dbSettings['BDD_PASS'] . " " . $this->dbSettings['BDD_NAME'] . "| sed 's/ AUTO_INCREMENT=[0-9]*\b//";*/

        $command = "mysqldump --no-data --skip-comments -h " . $this->dbSettings['BDD_HOST'] . " -u " . $this->dbSettings['BDD_USER'] . " -p" . $this->dbSettings['BDD_PASS'] . " " . $this->dbSettings['BDD_NAME'];

        $res = shell_exec($command);

        // remove the autoincrement info
        $res = preg_replace("/ AUTO_INCREMENT=[0-9]*/", "", $res);

        return $res;
    }

    /**
     * Ensures the tables in $tablesNames exists in the DB
     * @param  array $tablesNames the tables
     * @throws Exception if a table is not in the DB
     */
    private function validateTables(array $tablesNames)
    {
        if (empty($tablesNames)) {
            return;
        }

        $result = $this->db->query("SHOW TABLES");

        $existingTables = array();
        while ($row = $result->fetch_array(MYSQLI_NUM)) {
            $existingTables[] = $row[0];
        }

        foreach ($tablesNames as $key => $tableName) {
            if (!in_array($tableName, $existingTables)) {
                throw new \Exception($tableName . " does not exist in the database.");
            }
        }
    }
}
