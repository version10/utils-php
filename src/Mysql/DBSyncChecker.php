<?php

namespace Version10\Utils\Mysql;

use Guzzle\Http\Client;

/**
 * This class is useful for checking if two MySQL databases are synced and,
 * if not, show the differences between them
 *
 * Exemple usage :
 *
 *
 */
class DBSyncChecker
{
    /** Urls of the webservices to get the db structures */
    private $db1StructureGetterUrl;
    private $db2StructureGetterUrl;

    /**
     * Constructor
     */
    public function __construct($db1StructureGetterUrl, $db2StructureGetterUrl)
    {
        $this->db1StructureGetterUrl = $db1StructureGetterUrl;
        $this->db2StructureGetterUrl = $db2StructureGetterUrl;
    }

    public function getDiff()
    {

        // Create a client and provide a base URL
        $client = new Client($this->db1StructureGetterUrl);

        // Create a request with basic Auth
        $request = $client->get();

        // Send the request and get the response
        $response1 = $request->send();

        // Create a client and provide a base URL
        $client = new Client($this->db2StructureGetterUrl);

        // Create a request with basic Auth
        $request = $client->get();

        // Send the request and get the response
        $response2 = $request->send();


        // Options for generating the diff
        $options = array(
            //'ignoreWhitespace' => true,
            //'ignoreCase' => true,
        );

        // Initialize the diff class
        $diff = new \Diff(
            $response1->getBody(),
            $response2->getBody(),
            $options
        );

        //var_dump($diff);exit;
        return $diff;
    }
}
