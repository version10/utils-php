<?php

namespace Version10\Utils\Mysql;

/**
 * Adaptation de la classe Bdd utilisant l'extension mysql avec mysqli
 * Mise en place du design pattern Singleton pour éviter d'avoir plusieurs connexions par client
 *
 * Utilisation :
 * $Bdd = Bdd::getInstance();
 *
 * @author version 10
 */
class Bdd
{
    // Instance unique
    private static $instance = null;

    // Contient la connexion
    private $mysqli = null;

    /**
     * Constructeur
     * Privé : créé l'objet et la stocke dans instance
     */
    private function __construct()
    {
        $this->mysqli = new \mysqli(BDD_HOST, BDD_USER, BDD_PASS, BDD_NAME);

        if ($this->mysqli->connect_error) {
            throw new \Exception('Erreur de connexion ('.$this->mysqli->connect_errno.') '.$this->mysqli->connect_error);
        }

        // ne pas faire ça (ci-dessous), utiliser characterSetName('utf8') à la place (plus loin)
        // $this->mysqli->query("SET NAMES 'UTF8'");
        // voir http://www.php.net/manual/en/mysqli.set-charset.php

        $this->characterSetName('utf8');
        // print_r( $this->mysqli->get_charset() );
    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
        $this->mysqli->close();
        self::$instance = null;
    }

    /**
     * Récupère l'instance si elle existe. Si elle n'existe pas, créé l'objet Bdd et retourne la connexion
     * @return Instance de l'objet
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Change le charset si fourni en param et retourne le charset courant
     * @param string $char_set
     */
    public function characterSetName($char_set = null)
    {
        if (!empty($char_set) && ($char_set == 'utf8' || $char_set == 'latin1')) {
            $this->mysqli->set_charset($char_set);
        }

        return $this->mysqli->character_set_name();
    }

    /**
     * Change la base de donnée MySQL
     * @param String $dbname
     */
    public function selectDb($dbname)
    {
        if (empty($dbname)) {
            throw new \Exception('Le nom de la BDD est vide');
        }

        if ($this->_mysqlConnection == false) {
            throw new \Exception('La connection n\'est pas établie');
        }

        return $this->mysqli->select_db($dbname);
    }

    /**
     * Échappe un array ou un string à l'aide de la fonction real_escape_string
     * @param array|string $input Valeur(s) à échappé à l'aide de real_escape_string
     * @return array|string Valeur(s) une fois échappé
     */
    public function safe($input)
    {
        if (is_array($input)) {
            foreach ($input as $cle => $valeur) {
                $input[$cle] = trim($this->mysqli->real_escape_string($valeur));
            }

            return $input;
        } else {
            return trim($this->mysqli->real_escape_string($input));
        }
    }

    /**
     * Exécute la requéte SQL
     * @param string $sql Requête SQL échappé
     */
    public function query($sql)
    {
        return $this->mysqli->query($sql);
    }

    /**
     * Fait une requete et retourne le résultat sous forme de tableau associatif facile à manipuler
     * @param string $sql Requete SQL
     * @return array|bool FALSE si le requete cause une erreur
     */
    public function queryArray($sql)
    {
        $result = $this->mysqli->query($sql);

        if ($result === false) {
            throw new \Exception('Erreur lors de l\'éxécution de la requête : '.$this->mysqli->error);
        }

        // Retourne un tableau associatif
        $results = array();
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        return $results;

        // Retourne un tableau associatif
        // commenté car ne fonctionne pas par défaut sur tous les serveurs
        //return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * Fait une requete et retourne le premier résultat
     * @param string $sql Requete SQL
     * @return array|bool FALSE si la requete cause une erreur
     */
    public function queryFirst($sql, $return_string = false)
    {
        $result = $this->mysqli->query($sql);

        if ($result === false) {
            throw new \Exception('Erreur lors de l\'éxécution de la requête : '.$this->mysqli->error);
        } else {
            // Retourne un tableau associatif, dans le cas ou la variable ne se fait pas écraser
            $return = $result->fetch_array(MYSQLI_ASSOC);

            if ($return_string == true && count($return) == 1) {
                //Retourne une string
                $return = current($return);
            } elseif ($return_string == true && count($return) > 1) {
                throw new \Exception('Plus d\'un champs a &eacute;t&eacute; re&ccedil;u dans le r&eacute;sultat de la requ&ecirc;te');
            }
        }

        return $return;
    }

    /**
     * Build a safe query to execute later
     *
     * @param string args[0] Query to parse
     * @param string|array args[0] Array list of values to replace in query or string #1
     * @param string args[0..n] Facultative strings to replace for value #n
     * @example
     *      buildQuery("SELECT #0# WHERE #1#", "test", "test2");
     *      buildQuery("SELECT #test1# WHERE #test2#", array("test1", "test2"));
     *
     * @return string Safe query to execute
     */
    public function buildQuery()
    {
        $args = func_get_args();
        $nArgs = count($args);
        $str = $args[0];

        if ($nArgs == 2 && is_array($args[1])) {
            $args = $args[1];
        } else {
            array_shift($args);
        }

        $nArgs = count($args);

        foreach ($args as $arg => $val) {
            $str = str_replace("#".$arg."#", $this->safe($val), $str);
        }

        return $str;
    }

    /**
     * Retourne le dernier ID généré
     */
    public function getLastInsertId()
    {
        return $this->mysqli->insert_id;
    }

    /**
     * Retourne l'instance mysqli
     */
    public function getMysqliInstance()
    {
        return $this->mysqli;
    }
}
