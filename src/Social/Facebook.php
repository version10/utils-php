<?php

namespace Version10\Utils\Social;

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('ROOT')) {
	define('ROOT', "../");
}

class Facebook
{
	private static $SCRIPT_XFBML_CHARGER = false;


	static function GetXFBMLScript() {
		if (!self::$SCRIPT_XFBML_CHARGER) {
			return '<div id="fb-root"></div><script src="http://connect.facebook.net/fr_FR/all.js#xfbml=1"></script>';
			self::$SCRIPT_CHARGER = true;
		}
		else {
			return '';
		}
	}

	/**
	 * Crée un like button selon les parametres spécifié
	 * @param array $customParams Liste des paramètres personnalisé
	 *		href : Adresse cible du bouton
	 *		layout : standard, button_count, box_count
	 *		show_faces : true, false
	 *		width : Largeur du iframe
	 *		height : Hauteur du iframe
	 *		action : like, recommend
	 *		font : arial, lucida grande, segoe ui, tahoma, trebuchet ms, verdana
	 *		colorscheme : light, dark
	 */
	static function LikeButton($customParams = array()) {
		$params = array(
			'href' => null,
			'layout' => 'standard',
			'show_faces' => 'true',
			'width' => 450,
			'height' => 80,
			'action' => 'like',
			'font' => null,
			'colorscheme' => 'light'//,
			//'send' => false
		);

		foreach ($customParams as $nom => $valeur) {
			$params[$nom] = $valeur;
		}

		$urlParams = http_build_query($params, '', '&amp;');
		$url = 'http://www.facebook.com/plugins/like.php?'.$urlParams;
		$html = '<iframe src="'.$url.'" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'.$params['width'].'px; height:'.$params['height'].'px;" allowTransparency="true"></iframe>';

		return $html;
	}

	/**
	 * Retourne le lien de partage facebook de la page spécifié avec le texte passé en paramètre par défaut comme commentaire
	 * @param array $customParams Liste des paramètres personalisés
	 *		u : Url de la page à commencer, page actuelle par défaut
	 *		t : Texte de partage proposé à l'utilisateur
	 */
	static function ShareUrl($customParams = array()) {
		$currentUrl = 'http'.((!empty($_SERVER['HTTPS'])) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$params = array(
			'u' => $currentUrl,
			't' => null
		);

		foreach ($customParams as $nom => $valeur) {
			$params[$nom] = $valeur;
		}

		$urlParams = http_build_query($params);
		$url = 'http://www.facebook.com/sharer.php?'.$urlParams;

		return $url;
	}

	/**
	 * Crée un bouton html qui permet de partager un lien.
	 * @param array $customParams Liste des paramètres personalisés
	 *		href : url à partager
	 *		text : Texte proposé
	 * @param string $texte Texte du bouton
	 * @param bool $preventOnClick Prévient l'application des évènements de clique par défaut du bouton
	 * @param bool $preventStyle Prévient l'application des styles par défaut au bouton
	 */
	static function ShareButton($customParams = array(), $texte = 'Partager', $preventOnClick = false, $preventStyle = false) {
		$currentUrl = 'http'.((!empty($_SERVER['HTTPS'])) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$url = self::ShareUrl($customParams);
		$params = array(
			'href' => $currentUrl,
			'text' => '',
		);

		$url = sprintf(
			'http://facebook.com/sharer.php?u=%s&amp;t=%s',
			rawurlencode($params['href']),
			rawurlencode($params['text'])
		);

		$onclick = ($preventOnClick) ? '' : "window.open(this.href, 'partager sur facebook', 'width=600,height=400'); return false;";
		$styleLien = ($preventStyle) ? '' : "
			width:60px;
			background-image:url('http://static.ak.fbcdn.net/images/connect_sprite.png');
			background-repeat:no-repeat;
			background-position:-2px -233px;
			background-color:#5F78AB;
			color:white;
			font-size:10px;
			font-family:'lucida grande',tahoma;
			font-weight:bold;
			border:1px solid #29447E;
			padding:2px 0px;
			padding-left:16px;
			text-decoration:none;
			*padding-top:0px; *padding-bottom:0px;
		";
		$styleSpan = ($preventStyle) ? '' : "border-top:1px solid #879ac0; padding:1px 6px; line-height:18px;";

		$html = '
			<a href="'.$url.'" class="facebook-button-custom" style="'.$styleLien.'" onclick="'.$onclick.'"><span style="'.$styleSpan.'">'.$texte.'</span></a>
		';

		return $html;
	}

	/**
	 * @param array $customParams Liste des paramètres personalisés
	 *		url : Url de la page à commenter, page actuelle par défaut
	 *		text : Texte de partage proposé à l'utilisateur
	 * @param string $type Type du bouton : Facebookbutton, button_count, box_count, icon_link
	 */
	static function OldShareButton($customParams = array(), $type = 'button') {
		$url = self::ShareUrl($customParams);
		$html = '
		<a name="fb_share" type="'.$type.'" href="'.$url.'">
			Share
		</a>
		<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>';

		return $html;
	}

	/**
	 * Crée un like box selon les parametres spécifié
	 * @param array $customParams Liste des paramètres personnalisé
	 *		href : Adresse cible du likebox : http://www.facebook.com/surlerythme
	 *		width : Largeur du iframe
	 *		height : Hauteur du iframe
	 *		colorscheme : light, dark
	 *		show_faces : true, false
	 *		stream : true, false
	 *		header : true, false
	 */
	static function LikeBox($customParams = array()) {
		$params = array(
			'href' => null,
			'width' => 292,
			'height' => 427,
			'colorscheme' => 'light',
			'show_faces' => 'true',
			'stream' => 'true',
			'header' => 'true',
		);

		foreach ($customParams as $nom => $valeur) {
			if ($nom == 'href' && strpos($valeur, 'http://facebook.com/') === false) {
				$valeur = 'http://facebook.com/'.$valeur;
			}

			$params[$nom] = $valeur;
		}

		// réduit de 1 la largeur et hauteur afin d'éviter que la bordure de FB soit à l'extérieur (BUG de leur coté)
		$params['width'] -= 1;
		$params['height'] -= 1;

		$urlParams = http_build_query($params, '', '&amp;');

		// remet la bonne largeur et hauteur au iframe
		$params['width'] += 1;
		$params['height'] += 1;

		$url = 'http://www.facebook.com/plugins/likebox.php?'.$urlParams;
		$html = '<iframe src="'.$url.'" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'.$params['width'].'px; height:'.$params['height'].'px;" allowTransparency="true"></iframe>';

		return $html;
	}

	/**
	 * Crée les meta tags open graph utiliser par facebook pour afficher les infos lorsqu'un lien est poster / partager sur facebook
	 * @param array $customParams Liste des tags à ajouter et leur valeur sous la forme d'un array associatif
	 *		title : Titre de la page (human readable)
	 *		type : Type de la page : website, bloc, article, tv_show, movie, ... voir http://developers.facebook.com/docs/reference/plugins/like/
	 *		url : Url permanent de la page
	 *		image : Image représentant la page. Minomum 50x50px
	 *		site_name : Titre du site (human readable)
	 */
	static function OpenGraphTags($customParams = array()) {
		$currentUrl = 'http'.((!empty($_SERVER['HTTPS'])) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$html = '';
		$params = array(
			'title' => null,
			'type' => 'website',
			'url' => $currentUrl,
			'image' => null,
			'site_name' => null
		);

		foreach ($customParams as $nom => $valeur) {
			$params[$nom] = $valeur;
		}

		foreach ($params as $nom => $valeur) {
			if ($valeur) {
				$html .= '<meta property="og:'.$nom.'" content="'.$valeur.'" />'."\n";
			}
		}

		return $html;
	}

	/**
	 * Bloc de commentaire géré par facebook relié à une adresse
	 * @param array $customParams Liste des tags à ajouter et leur valeur sous la forme d'un array associatif
	 *		href : Adresse de la page à commenter
	 *		width : largeur du bloc de commentaire
	 *		num_posts : nombre de posts à afficher
	 *		colorscheme : thème de couleur (light, dark)
	 */
	static function Comments($customParams = array()) {
		$currentUrl = 'http'.((!empty($_SERVER['HTTPS'])) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$params = array(
			'href' => $currentUrl,
			'width' => 600,
			'num_posts' => 2,
			'colorscheme' => 'light',
		);

		foreach ($customParams as $nom => $valeur) {
			$params[$nom] = $valeur;
		}

		$html = self::GetXFBMLScript().'<fb:comments';
		foreach ($params as $nom => $param) {
			$html .= " $nom=\"$param\"";
		}
		$html .= '></fb:comments>';
		return $html;
	}
}

