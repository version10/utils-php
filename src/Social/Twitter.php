<?php

namespace Version10\Utils\Social;

//OAUTH CLASS, Required since Twitter API 1.1
require_once(__DIR__ ."/twitteroauth/twitteroauth.php");

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('ROOT')) {
	define('ROOT', "../");
}


class Twitter
{
	private static $CACHE_FOLDER = 'cache'; // sous dossier dans lequel stocker les fichiers de cache
	private static $CACHE_SEARCH_TIME = 300; // minutes de cache (60 * minutes)
	private static $CACHE_SEARCH_EXTENSION = '.tweets'; // extension des fichiers cache de requête à l'api de recherche

	/**
	 * Initialise les valeurs statique complexe de la classe. Appelé automatiquement après la déclaration de la classe
	 */
	public static function Init() {
		// dossier cache est relatif à l'emplacement actuel du fichier
		self::$CACHE_FOLDER = dirname(__FILE__).DS.self::$CACHE_FOLDER.DS;

		if (!is_dir(self::$CACHE_FOLDER)) {
			mkdir(self::$CACHE_FOLDER);
		}
	}

	/**
	 * Retourne le lien de partage facebook de la page spécifié avec le texte passé en paramètre par défaut comme commentaire
	 * @param array $customParams Liste des paramètres personalisés
	 *		url : Url à tweeter
	 *		via : Nom de l'utilisateur à qui attribué le tweet
	 *		text : Text par défaut proposé pour le tweet
	 *		related : Accounts reliés
	 *		count : position de la box count (none, horizontal, vertical)
	 *		lang : langage du bouton
	 *		counturl : l'url aquel le tweet appartient
	 */
	public static function ShareUrl($customParams = array()) {
		$currentUrl = 'http'.((!empty($_SERVER['HTTPS'])) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$params = array(
			'url' => $currentUrl,
			'via' => null,
			'text' => null,
			'related' => null,
			'count' => 'none',
			'lang' => 'fr',
			'counturl' => $currentUrl
		);

		foreach ($customParams as $nom => $valeur) {
			$params[$nom] = $valeur;
		}

		$urlParams = http_build_query($params);
		//$url = 'http://twitter.com/share?'.$urlParams;
		$url = 'http://twitter.com/intent/tweet?'.$urlParams;

		return $url;
	}

	/**
	 * Crée un like button selon les parametres spécifié
	 * @param array $customParams Voir self::ShareUrl
	 * @see self::ShareUrl
	 */
	public static function ShareButton($customParams = array(), $preventStyle = false, $preventClick = false, $preventOver = false) {
		$url = self::ShareUrl($customParams);

		$style = "
			padding:0px 33px;
			font-size:18px;
			background-image:url('http://platform0.twitter.com/widgets/images/tweet_fr.png');
		";
		$style2 = "position:absolute; top:-9999px;";

		$onclick = "window.open(this.href, 'partage twitter', 'width=600,height=400'); return false;";

		$onmouseover = "this.style.backgroundPosition = '0px -21px'";
		$onmouseout = "this.style.backgroundPosition = '0px 0px'";

		$html = '
			<a href="'.$url.'"
				class="twitter-button-custom"
				target="_blank"
				style="'.$style.'"
				onmouseover="'.$onmouseover.'"
				onmouseout="'.$onmouseout.'"
				onclick="'.$onclick.'">
				<span style="'.$style2.'">Tweeter</span>
			</a>
		';

		return $html;
	}

	/**
	 * Crée un like button selon les parametres spécifié
	 * @param array $customParams Voir self::ShareUrl
	 * @see self::ShareUrl
	 */
	public static function ShareButtonWidget($customParams = array()) {
		$url = self::ShareUrl($customParams);
		$html = '
			<a href="'.$url.'" class="twitter-share-button" target="_blank">Tweet</a>
			<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
		';

		return $html;
	}

	/**
	 * Récupère les tweets avec l'api de recherche twitter selon les paramètres
	 * @param string $queryStr Utilisateur twitter à rechercher
	 * @param int $nb Nombre de résultats à aller chercher (min = 1, max = 100)
	 * @param bool $reply Recherche aussi des reply à l'utilisateur
	 */
	public static function Fetch($type = 'recherche', $queryStr = '', $nb = 10) {
		// remplace les espaces dans le username par des '-'
		$fichier_prefix = preg_replace('/ /', '-', $queryStr);

		// référance au temps actuel
		$t = time();

		// queryStr-1302111963.tweets
		$fichierCache = glob(self::$CACHE_FOLDER.$fichier_prefix.'-*'.self::$CACHE_SEARCH_EXTENSION);

		// Analyse pour un fichier cache assez récent
		// if (count($fichierCache)) {
		// 	$fichierCache = basename($fichierCache[0]);
		// 	$tempsCache = substr($fichierCache, strlen($fichier_prefix.'-'), -strlen(self::$CACHE_SEARCH_EXTENSION));

		// 	// si le cache est assez récent
		// 	if ((int) $tempsCache + self::$CACHE_SEARCH_TIME >= $t) {
		// 		return json_decode(file_get_contents(self::$CACHE_FOLDER.$fichierCache));
		// 	} else { // si le cache est trop ancient
		// 		unlink(self::$CACHE_FOLDER.$fichierCache);
		// 	}
		// }

		### AUTHENTICATE | TWITTER API 1.1 ###
		$connection = new \TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_TOKEN_SECRET);

		if ($type == 'recherche') {

			//DEPRECATED | TWITTER API 1.0
			//$url = 'http://search.twitter.com/search.json?q='.$queryStr.'&rpp='.$nb;
			//$json = @file_get_contents($url);

			$twitter_call_parameters = array(	"q"		=>	$queryStr,
												"count"	=>	$nb			);

			$json = $connection->get("search/tweets", $twitter_call_parameters);

			if (!$json) {
				return false;
			}

			$tweets = json_decode($json);

		} else if ($type == 'user') {

			//DEPRECATED / TWITTER API 1.0
			//$url = 'http://api.twitter.com/1/statuses/user_timeline.json?screen_name='.$queryStr.'&count='.$nb;
			//$jsonUser = @file_get_contents($url);

			$twitter_call_parameters = array(	"screen_name"	=>	$queryStr,
												"count"			=>	$nb			);

			$jsonUser = $connection->get("statuses/user_timeline", $twitter_call_parameters);

			if (!$jsonUser) {
				return false;
			}

			if (is_string($jsonUser) && (is_object(json_decode($jsonUser)) || is_array(json_decode($jsonUser)) ) ){
				$tweetsUser = json_decode($jsonUser);
			} else{
				$tweetsUser = $jsonUser;
			}

			$tweets = (object) array('results' => array());

			foreach ($tweetsUser as $i => $tweet) {
				$tweets->results[] = (object) array(
					'from_user_id_str' => $tweet->user->id_str,
					'id_str' => $tweet->id_str,
					'from_user' => $queryStr,
					'text' => $tweet->text,
				);
			}

			$json = json_encode($tweets);

		} else {
			return false;
		}

		if ($tweets) {
			$f = fopen(self::$CACHE_FOLDER.$fichier_prefix.'-'.$t.self::$CACHE_SEARCH_EXTENSION, 'w+');
			fwrite($f, $json);
			fclose($f);
		}

		return $tweets;
	}
}

Twitter::Init();
