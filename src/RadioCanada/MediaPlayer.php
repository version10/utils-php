<?php

namespace Version10\Utils\RadioCanada;

/**
 * Utils related to the Radio-Canada media player
 */
class MediaPlayer
{
    /**
     * Generates the script tag needed to display the radio-canada player
     * @param  string  $id    media id
     * @param  integer $width player width
     * @return string         html containing the script tag to render the player
     */
    public function getVideoPlayerScript($id, $width = 400)
    {
        if (!empty($width)) {
            $width = (int) $width;
            $height = round(($width * 360) / 640);

            $dimensionParams = ", 'width':'".$width."', 'height':'".$height."'";
        } else {
            $dimensionParams = '';
        }

        $playerSRC = "http://api.radio-canada.ca/cav/v1/artvca/?pji={'appCode': 'medianet', 'idMedia': '".htmlspecialchars(urlencode($id))."', 'Params':{'autoPlay': false ".$dimensionParams."}}";

        return '<div style="margin-bottom:5px;"><script src="'.$playerSRC.'" type="text/javascript"></script></div>';
    }
}
