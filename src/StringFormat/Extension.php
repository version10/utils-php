<?php

namespace Version10\Utils\StringFormat;

/**
 * Utils related to strings format
 */
class Extension
{
    /**
     * Récupère l'extension du fichier
     * @param $f string Nom du fichier
     * @return string Extension
     */
    public function getExtension($f, $avecPoint = true)
    {
        $extension = strrchr($f, '.');

        if ($avecPoint) {
            return $extension;
        } else {
            return substr($extension, 1);
        }
    }

    /**
     * Change l'extension du fichier
     * @param $f string Nom du fichier
     * @param $extension string Nouvelle extension
     * @return string Nom du fichier avec la nouvelle extension
     */
    public function changeExtension($f, $extension)
    {
        if ($extension[0] !== '.') {
            $extension = '.'.$extension;
        }

        return str_replace($this->getExtension($f), $extension, $f);
    }

    /**
     * S'assure que l'extention du fichier est en minuscule
     * et remplace les .jpeg en .jpg
     * @param string $fileName nom du fichier
     * @return string nom du fichier propre
     */
    public function sanitizeExtension($fileName)
    {
        $newExtention = strtolower($this->getExtension($fileName, false));
        if ($newExtention == 'jpeg') {
            $newExtention = 'jpg';
        }

        return $this->changeExtension($fileName, $newExtention);
    }

    /**
     * Enlève l'extention du nom de fichier
     * @param string $fileName
     * @return string the filename without extension
     */
    public function removeExtension($fileName)
    {
        $extensionPos = strrpos($fileName, '.');

        return substr($fileName, 0, $extensionPos);
    }

    /**
     * Ajoute un suffix au nom du fichier avant l'extension
     * @param $f string Nom du fichier
     * @param $suffix string Chaine de texte à ajouter
     * @return string Nom du fichier avec l'ajout du texte
     */
    public function suffixFilename($f, $suffix = 'tmb', $separator = '-')
    {
        $extension = $this->getExtension($f);

        return str_replace($extension, $separator.$suffix.$extension, $f);
    }

    /**
     * Ajoute un prefix au nom du fichier en début de fichier
     * @param $f string Nom du fichier
     * @param $prefix string Chaine de texte à ajouter
     * @return string Nom du fichier avec l'ajout du texte
     */
    public function prefixFilename($f, $prefix = 'tmb', $separator = '-')
    {
        return $prefix.$separator.$f;
    }

    /**
     * Renvoi true si la chaîne passée en paramètre débute par http://
     * @param string $url
     * @return boolean
     */
    public function startsWithHttpProtocol($url)
    {
        // Check param
        if (empty($url)) {
            return false;
        }

        if (preg_match('/^http:\/\//', $url)) {
            return true;
        }

        return false;
    }
}
