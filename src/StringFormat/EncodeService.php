<?php

namespace Version10\Utils\StringFormat;

/**
 * Utils related to strings format
 */
class EncodeService
{
	/**
	 * Version ISO de la fonction json_encode qui n'est que utf8
	 * Convertie tout le contenu (string ou array récursivement) en utf8 avant de faire l'encode
	 * puis reconverti le json en ISO
	 * @param $content String || Array Contenu à encoder en json
	 * @return String Version json de $content
	 */
	public function jsonEncodeIso($content)
	{
		if (is_array($content)) {
			$content = $this->utf8_encode_array($content);
			return utf8_decode(json_encode($content));
		}

		return utf8_decode(json_encode(utf8_encode($content)));
	}

	/**
	 * Encode en utf8 les clés et les valeurs d'un array
	 * @param $array Array Array à convertir
	 * @param $recursive Boolean Encode de façon récursive dans le cas ou le array en contient d'autres
	 * @return Array Encoded array
	 */
	public function utf8EncodeArray($array, $recursive = true)
	{
		$finalArray = array();

		foreach ($array as $key => $value) {
			if (is_array($value)) {
				if ($recursive) {
					$finalArray[utf8_encode($key)] = $this->utf8_encode_array($value);
				} else {
					$finalArray[utf8_encode($key)] = $value;
				}
			} else {
				$finalArray[utf8_encode($key)] = utf8_encode($value);
			}
		}

		return $finalArray;
	}
}
