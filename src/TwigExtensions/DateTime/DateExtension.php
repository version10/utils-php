<?php

namespace Version10\Utils\TwigExtensions\DateTime;


/*
 * Twig extension related to Date
 */
class DateExtension extends \Twig_Extension
{
    private $dateService;

    public function __construct()
    {
        $this->dateService = new \Version10\Utils\DateTime\DateService();
    }

    //####################################################################################//
    // Twig methodes

    public function getName()
    {
        return 'DateExtension';
    }

    /**
     * Retourne une liste de fonctions qui seront ajoutés à la liste de fonctions de Twig par défauts.
     *
     * @return array Un tableau de fonctions
     */
    public function getFunctions()
    {
        return array(
            //new \Twig_SimpleFunction('exemple', array($this, 'exemple')),
        );
    }

    /**
     * Retourne une liste de filtres qui seront ajoutés à la liste de filtes de Twig par défauts.
     *
     * @return array Un tableau de filtres
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('moisToString', array($this->dateService, 'moisToString')),
            new \Twig_SimpleFilter('jourToString', array($this->dateService, 'jourToString')),
            new \Twig_SimpleFilter('dateToStringFr', array($this->dateService, 'dateToStringFr')),
            new \Twig_SimpleFilter('dateTimeToStringFr', array($this->dateService, 'dateTimeToStringFr')),
            new \Twig_SimpleFilter('int2readable', array($this->dateService, 'int2readable')),
        );
    }

    // FIN - Twig methodes
    //####################################################################################//
}
