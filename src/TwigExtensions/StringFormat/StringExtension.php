<?php

namespace Version10\Utils\TwigExtensions\StringFormat;


/*
 * Twig extension related to Date
 */
class StringExtension extends \Twig_Extension
{
    private $stringService;

    public function __construct(){
        $this->stringService = new \Version10\Utils\StringFormat\Extension();
    }

    //####################################################################################//
    // Twig methodes

    public function getName()
    {
        return 'StringExtension';
    }

    /**
     * Retourne une liste de fonctions qui seront ajoutés à la liste de fonctions de Twig par défauts.
     *
     * @return array Un tableau de fonctions
     */
    public function getFunctions()
    {
        return array(
            //new \Twig_SimpleFunction('exemple', array($this, 'exemple')),
        );
    }

    /**
     * Retourne une liste de filtres qui seront ajoutés à la liste de filtes de Twig par défauts.
     *
     * @return array Un tableau de filtres
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('getExtension', array($this->stringService, 'getExtension')),
            new \Twig_SimpleFilter('changeExtension', array($this->stringService, 'changeExtension')),
            new \Twig_SimpleFilter('sanitizeExtension', array($this->stringService, 'sanitizeExtension')),
            new \Twig_SimpleFilter('removeExtension', array($this->stringService, 'removeExtension')),
            new \Twig_SimpleFilter('suffixFilename', array($this->stringService, 'suffixFilename')),
            new \Twig_SimpleFilter('prefixFilename', array($this->stringService, 'prefixFilename')),
            new \Twig_SimpleFilter('startsWithHttpProtocol', array($this->stringService, 'startsWithHttpProtocol')),
        );
    }

    // FIN - Twig methodes
    //####################################################################################//
}