<?php

namespace Version10\Utils;

/**
*   This class allows you to :
*   - use constants that are not only scalar and null values (arrays for example)
*   as you can with the define() function.
*   - constrain them into a "protected" environment, this can
*   avoid conflicts will globally defined constants
*
*
*   Usage and example :
*   $constants = new Constants();
*   $constants->set('countries', array('canada', 'denmark'));
*   $constants->get('countries'); //returns array('canada', 'denmark')
*
*   //this will fail since 'countries' is already registered
*   $constants->set('countries', array('usa', 'denmark')); //throw exception
*
*   $constants->get('countries'); //returns array('canada', 'denmark')
*/
class Constants
{
    /**
     * This will hold all the constants
     * @var array
     */
    private $constants;

    /**
     * Constructor
     */
    public function __construct()
    {
        $constants = array();
    }

    /**
     * Return the constant value
     *
     * @param String $name constant name
     *
     * @return mixed        constant value
     */
    public function get($name)
    {
        if (array_key_exists($name, $this->constants)) {
            return $this->constants[$name];
        } else {
            return null;
        }
    }

    /**
     * Set the constant if it does not already exists
     *
     * @param String $name  constant name
     * @param mixed  $value constant value
     *
     * @return void
     */
    public function set($name, $value)
    {
        if (!array_key_exists($name, $this->constants)) {
            $this->constants[$name] = $value;
        } else {
            throw new Exception('Constant already defined');
        }
    }
}
