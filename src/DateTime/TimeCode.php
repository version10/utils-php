<?php

namespace Version10\Utils\DateTime;

/**
 *  Utils related to the timecode formatage
 */

class TimeCode
{
	public static function sec2timecode($seconds)
	{
		$hour = floor($seconds / 3600);
		$timeleft = $seconds % 3600;
		$minute = floor($timeleft / 60);
		$second = $timeleft % 60;

		$timecode = $this->twoDigit($hour).':'.$this->twoDigit($minute).':'.$this->twoDigit($second);

		return $timecode;
	}

	public static function min2timecode($minuts)
	{
		$hour = floor($minuts / 60);
		$timeleft = $minuts % 60;
		$minute = $timeleft;
		$second = 0;

		$timecode = $this->twoDigit($hour).':'.$this->twoDigit($minute).':'.$this->twoDigit($second);

		return $timecode;
	}

	public static function timecode2sec($timecode)
	{
		$time = explode(':', $timecode);

		for ($i = count($time); $i < 3; $i++) {
			array_unshift($time, '0');
		}

		return ($time[0] * 3600) + ($time[1] * 60) + $time[2];
	}

	public function twoDigit($dg)
	{
		$out = '';

		if ($dg < 1) {
			$out = '00';
		} elseif ($dg < 10) {
			$out = '0'.$dg;
		} else {
			$out = $dg;
		}

		return $out;
	}
}
