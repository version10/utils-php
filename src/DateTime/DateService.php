<?php

namespace Version10\Utils\DateTime;

/**
 * Utils related to the Date and Time formatage
 */

class DateService
{

    /**
     * Renvoie le nom du mois en français à partir de nom numéro
     * @param int $n numéro du mois
     * @return string nom du mois
     */
    public function moisToString($n)
    {
        if ($n > 12) {
            $n = date('n', $n);
        }

        $a = array(
            1 => 'janvier',
            2 => 'février',
            3 => 'mars',
            4 => 'avril',
            5 => 'mai',
            6 => 'juin',
            7 => 'juillet',
            8 => 'août',
            9 => 'septembre',
            10 => 'octobre',
            11 => 'novembre',
            12 => 'décembre',
        );

        return $a[(int)$n];
    }

    /**
     * Renvoie le nom du jour en français à partir de nom numéro
     * @param int $w
     * @return string nom du jour
     */
    public function jourToString($w)
    {
        if ($w > 7) {
            $w = date('w', $w);
        }

        $a = array(
            1 => 'lundi',
            2 => 'mardi',
            3 => 'mercredi',
            4 => 'jeudi',
            5 => 'vendredi',
            6 => 'samedi',
            7 => 'dimanche',
        );

        return $a[(int)$w];
    }

    public function dateToStringFr($date, $show_day = true, $show_month = true, $show_year = true)
    {
        $timestamp = (is_numeric($date)) ? $date : strtotime($date);

        $out = '';

        if ($show_day == true) {
            $out .= date('j', $timestamp).' ';
        }

        if ($show_month == true) {
            $out .= $this->moisToString(date('n', $timestamp)).' ';
        }

        if ($show_year == true) {
            $out .= date('Y', $timestamp);
        }

        $out = trim($out);

        return $out;
    }

    public function dateTimeToStringFr($date)
    {
        if ($date != '0000-00-00 00:00:00') {
            $timestamp = (is_numeric($date)) ? $date : strToTime($date);

            return date('j', $timestamp).' '.$this->moisToString(date('n', $timestamp)).' '.date('Y', $timestamp).', '.date('H:i:s', $timestamp);
        }

        return;
    }

    public function int2readable($date, $format = '\l\e d-m-Y à H:i')
    {
        $out = '';

        if (!empty($date)) {
            $out = date($format, $date);
        } else {
            $out = '-';
        }

        return $out;
    }
}
