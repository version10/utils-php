<?php

namespace Version10\Utils\DateTime;

/**
 * Utils related to the time formatage
 */

class TimeService
{

    /**
     * Renvoie le nom du mois en français à partir de nom numéro
     * @param int $n numéro du mois
     * @return string nom du mois
     */
    public static function getTimeNow()
    {
        if (defined('NOW_OVERRIDABLE_TIMESTAMP') && NOW_OVERRIDABLE_TIMESTAMP) {
            if (defined('OVERRIDDEN_TIMESTAMP') && OVERRIDDEN_TIMESTAMP) {
                return OVERRIDDEN_TIMESTAMP;
            } else {
                return time();
            }
        } else {
            if (defined('OVERRIDDEN_TIMESTAMP') && OVERRIDDEN_TIMESTAMP) {
                return date("Y-m-d H:i:s", OVERRIDDEN_TIMESTAMP);
            } else {
                return date("Y-m-d H:i:s");
            }
        }
    }

    public static function timeToStringFr($time)
    {
        if ($time != '') {
            $tmp = explode(':', $time);
            $out = (int)$tmp[0].'h'.$tmp[1];

            return $out;
        }
    }
}
