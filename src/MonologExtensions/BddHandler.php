<?php

namespace Version10\Utils\MonologExtensions;

use Monolog\Logger;
use Version10\Utils\Mysql\Bdd;

/**
 * Enable the logging of all errors and exceptions into the database through the Bdd object
 * @see https://github.com/Seldaek/monolog/blob/master/doc/extending.md
 */
class BddHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    private $initialized = false;
    private $Bdd;

    public function __construct(Bdd $Bdd, $level = Logger::DEBUG, $bubble = true)
    {
        $this->Bdd = $Bdd;
        parent::__construct($level, $bubble);
    }

    protected function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        $sql =
            "INSERT INTO monolog (channel, level, message, time) VALUES ("
            ."'". $this->Bdd->safe($record['channel']) . "', "
            ."'". $this->Bdd->safe($record['level']) . "', "
            ."'". $this->Bdd->safe($record['formatted']) . "', "
            ."'". $this->Bdd->safe($record['datetime']->format('Y-m-d H:i:s')) . "')";

        $this->Bdd->query($sql);
    }

    private function initialize()
    {
        $this->Bdd->query(
            'CREATE TABLE IF NOT EXISTS monolog ('
                .'`id` int(11) NOT NULL AUTO_INCREMENT, '
                .'`channel` VARCHAR(255), '
                .'`level` INTEGER, '
                .'`message` LONGTEXT, '
                .'`time` TIMESTAMP, '
                .'PRIMARY KEY (`id`)'
            .') '
            .'ENGINE=InnoDB DEFAULT CHARSET=utf8 '
            .'COMMENT = "Table de log des erreurs"'
        );

        $this->initialized = true;
    }
}
