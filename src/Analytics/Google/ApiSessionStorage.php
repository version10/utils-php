<?php

namespace Version10\Utils\Analytics\Google;
/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Simple classes to store a value in a PHP sessions or in browser cookies.
 * You can add your own classes to persist values through other mechanisms
 * like in a SQLLite database.
 * @author Nick Mihailovski (api.nickm@gmail.com)
 */

/**
 * Use PHP sessions to store values.
 */
class ApiSessionStorage {

  /**
   * Sets a value in a PHP session.
   * @param string $value The value to set in the session.
   */
  public function set($value) {
    $_SESSION['access_token'] = $value;
  }

  /**
   * @return string The value stored in the session.
   */
  public function get() {
    return $_SESSION['access_token'];
  }

  /**
   * Deletes the value from the session.
   */
  public function delete() {
    unset($_SESSION['access_token']);
  }
}
