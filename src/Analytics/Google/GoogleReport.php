<?php

namespace Version10\Utils\Analytics\Google;

class GoogleReport {

    private $ga;

    private $viewId;

    function __construct($service, $viewId) {
        $this->ga = $service->data_ga;
        $this->viewId = $viewId;
    }

    public function getReport($startDate, $endDate, $metrics, $optParams = array()) {
        $report = $this->ga->get(
            $this->viewId,
            $startDate,
            $endDate,
            $metrics,
            $optParams
        );

        return $report;
    }

}
