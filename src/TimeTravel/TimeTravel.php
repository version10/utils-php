<?php

namespace Version10\Utils\TimeTravel;

use Silex\Application;

/**
 * Utils related to the time formatage
 */
class TimeTravel
{
    /** @var Application */
    protected $app;

    protected $overriddenTime = null;

    public function __construct(array $config)
    {
        if ($config['override_time_to']) {
            $this->overriddenTime = strtotime($config['override_time_to']);
        } else {
            $this->overriddenTime = null;
        }
    }

    public static function timeToStringFr($time)
    {
        if ($time != '') {
            $tmp = explode(':', $time);
            $out = (int)$tmp[0] . 'h' . $tmp[1];

            return $out;
        }
    }

    /**
     *
     */
    public function getTimeNow()
    {
        if ($this->overriddenTime) {
            return date("Y-m-d H:i:s", $this->overriddenTime);
        } else {
            return date("Y-m-d H:i:s");
        }
    }
}
