<?php

namespace Version10\Utils\TimeTravel;

use Silex\Application;
use Silex\ServiceProviderInterface;

class TimeTravelServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['time_travel'] = $app->share(function ($app) {
            $app['time_travel.default_options'] = array(
                'override_time_to' => null,
            );

            $options = $app['time_travel.default_options'];

            if (isset($app['time_travel.options'])) {
                $options = array_replace($app['time_travel.default_options'], $app['time_travel.options']);
            }

            return new TimeTravel($options);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
